﻿using System;    
using UIKit;
using Alarm.Utilities;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using System.Linq;
using Xamarin.Forms;
using Alarm.iOS.DependencyService;

[assembly: Dependency(typeof(LocalNotification))]
namespace Alarm.iOS.DependencyService
{
    public class LocalNotification : iLocalNotification
    {
        const string NotificationKey = "LocalNotificationKey";
        public void Cancel(int id)
        {
            var notifications = UIApplication.SharedApplication.ScheduledLocalNotifications;    
            var notification = notifications.Where(n => n.UserInfo.ContainsKey(NSObject.FromObject(NotificationKey))).FirstOrDefault(n => n.UserInfo[NotificationKey].Equals(NSObject.FromObject(id)));    
            UIApplication.SharedApplication.CancelAllLocalNotifications();    
            if (notification != null)
            {    
                UIApplication.SharedApplication.CancelLocalNotification(notification);    
                UIApplication.SharedApplication.CancelAllLocalNotifications();    
            }
        }

        void iLocalNotification.LocalNotification(string title, string body, int id, DateTime alarmtime)
        {
            var notification = new UILocalNotification
            {
                AlertTitle = title,
                AlertBody = body,
                SoundName = "campalarm.caf",
                FireDate = alarmtime.ToNSDate(),
                //RepeatInterval = NSCalendarUnit.Minute
            };
            UIApplication.SharedApplication.ScheduleLocalNotification(notification);
        }
    }

    public static class DateTimeExtensions{    
            
        static DateTime nsUtcRef = new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        public static double SecondsSinceNSRefenceDate(this DateTime dt)
        {    
            return (dt.ToUniversalTime() - nsUtcRef).TotalSeconds;    
        }
        public static NSDate ToNSDate(this DateTime dt){    
            return NSDate.FromTimeIntervalSinceReferenceDate(dt.SecondsSinceNSRefenceDate());    
        }
    }    
}
