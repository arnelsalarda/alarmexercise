﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Alarm
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            var hourNow = DateTime.Now.Hour;
            var minuteNow = DateTime.Now.Minute;
            var secondNow = DateTime.Now.Second;
            Console.WriteLine("time is " + minuteNow);

            var time = timePicker.Time.Minutes;
            timePicker.PropertyChanged += (sender, e) =>
            {
                Console.WriteLine("time is " + timePicker.Time);
                var timerHour = timePicker.Time.Hours;
                var timerMinute = timePicker.Time.Minutes;
                var tempSecond = timePicker.Time.Seconds;
                var tempHour = (timerHour - hourNow) * 60;
                time = ((timerMinute - minuteNow) + tempHour) + (tempSecond / 60);
                Console.WriteLine("temp hour " + tempHour);
                Console.WriteLine("time " + time);
            };
            timeSwitch.PropertyChanged += (sender, e) =>
            {
                if (((Switch)sender).IsToggled)
                {
                    if (time < 0)
                    {
                        ((Switch)sender).IsToggled = false;
                    }
                    else
                    {
                        //CrossLocalNotifications.Current.Show("title", "body", 101, DateTime.Now.AddMinutes(time));
                        Console.WriteLine("sadsd" + time);
                    }
                }
            };
        }
    }
}
