﻿using System;
using SQLite;

namespace Alarm.Models
{
    public class Alarm
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public int secondsToAlarm { get; set; }
        public TimeSpan alarmtime { get; set; }
        public bool isToggled { get; set; }
        public bool didStart { get; set; }
        public bool isFinish { get; set; }
    }
}
