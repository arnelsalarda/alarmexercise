﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using SQLite;

namespace Alarm.Utilities.Helpers.DatabaseReader
{
    public class DatabaseReader : iDatabaseReader
    {
        readonly SQLiteAsyncConnection database;

        public static DatabaseReader dbReader;
        public static DatabaseReader GetInstance
        {
            get
            {
                if (dbReader == null)
                {
                    dbReader = new DatabaseReader(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TodoSQLite.db3"));
                }
                return dbReader;
            }
        }

        public DatabaseReader(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
        }

        public void CreateTable<ClassName>() where ClassName : new()
        {
            database.CreateTableAsync<ClassName>().Wait();
        }

        public async Task SaveItemAsync<ClassName>(object alarm, string tableName, int id) where ClassName : new()
        {
            var data = database.QueryAsync<ClassName>("SELECT * FROM [" + tableName + "] WHERE [id] = " + id);
            if (data.Result.Count == 0) 
            {
                await database.InsertAsync(alarm);
            }
            else
            {
                await database.UpdateAsync(alarm);
            }
        }

        public async Task<List<Models.Alarm>> FetchData()
        {
            database.CreateTableAsync<Models.Alarm>().Wait();
            return await database.Table<Models.Alarm>().ToListAsync();
        }

        public async Task DeleteItemAsync(object item)
        {
            await database.DeleteAsync(item);
        }
    }
}