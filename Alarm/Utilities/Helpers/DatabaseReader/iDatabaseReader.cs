﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Alarm.Utilities.Helpers.DatabaseReader
{
    public interface iDatabaseReader
    {
        void CreateTable<ClassName>() where ClassName : new();
        Task<List<Models.Alarm>> FetchData();
        Task SaveItemAsync<ClassName>(object className, string tableName, int id) where ClassName : new();
        Task DeleteItemAsync(object item);
    }
}
