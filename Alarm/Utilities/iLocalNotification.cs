﻿using System;
namespace Alarm.Utilities
{
    public interface iLocalNotification
    {
        void LocalNotification(string title, string body, int id, DateTime notifyTime);    
        void Cancel(int id);
    }
}
