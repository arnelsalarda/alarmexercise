﻿using System;
using Alarm.Utilities;
using Alarm.Utilities.Helpers.DatabaseReader;
using Xamarin.Forms;

namespace Alarm.Views
{
    public partial class AddAlarmPage : ContentPage
    {
        DatabaseReader databasReader = DatabaseReader.GetInstance;
        int autoIncId = 0;
        Models.Alarm alarm;
        int time = 0;

        public AddAlarmPage()
        {
            InitializeComponent();
        }

        void Time_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var hourNow = DateTime.Now.Hour;
            var minuteNow = DateTime.Now.Minute;
            var secondNow = DateTime.Now.Second;

            var timerHour = timePicker.Time.Hours;
            var timerMinute = timePicker.Time.Minutes;
            var tempHour = (timerHour - hourNow) * 60;

            time = (((timerMinute - minuteNow) + tempHour) * 60) - secondNow;
            Console.WriteLine("secs " + time);
        }

        async void SaveButton_Clicked(object sender, System.EventArgs e)
        {
            alarm = new Models.Alarm() { id = autoIncId, alarmtime = timePicker.Time, secondsToAlarm = time, isToggled=true, didStart=false, isFinish=false};
            DependencyService.Get<iLocalNotification>().LocalNotification("title", "body", autoIncId, DateTime.Now.AddSeconds(time));
            await databasReader.SaveItemAsync<Models.Alarm>(alarm, "Alarm", autoIncId);
            await DisplayAlert("Alarm", "Alarm added to list!", "Ok");
            Navigation.PopAsync();
            autoIncId++;
        }
    }
}
