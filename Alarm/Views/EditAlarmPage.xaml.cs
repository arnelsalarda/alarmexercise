﻿using System;
using System.Collections.Generic;
using Alarm.Utilities.Helpers.DatabaseReader;
using Xamarin.Forms;

namespace Alarm.Views
{
    public partial class EditAlarmPage : ContentPage
    {
        DatabaseReader databasReader = DatabaseReader.GetInstance;
        Models.Alarm alarm;
        int time = 0;

        public EditAlarmPage(Models.Alarm alarm)
        {
            BindingContext = alarm;
            InitializeComponent();
            this.alarm = alarm;
        }

        void Time_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var hourNow = DateTime.Now.Hour;
            var minuteNow = DateTime.Now.Minute;
            var secondNow = DateTime.Now.Second;

            var timerHour = timePicker.Time.Hours;
            var timerMinute = timePicker.Time.Minutes;
            var tempHour = (timerHour - hourNow) * 60;

            time = (((timerMinute - minuteNow) + tempHour) * 60) - secondNow;
            Console.WriteLine("secs " + time);
        }

        async void SaveButton_Clicked(object sender, System.EventArgs e)
        {
            alarm.id = alarm.id;
            alarm.secondsToAlarm = time;
            alarm.alarmtime = timePicker.Time;
            alarm.isToggled = true;
            alarm.didStart = false;
            await DisplayAlert("Alarm", "Alarm updated!", "Ok");
            await databasReader.SaveItemAsync<Models.Alarm>(alarm, "Alarm", alarm.id);
            Navigation.PopAsync();
        }

        async void DeleteButton_CLicked(object sender, System.EventArgs e)
        {
            await DisplayAlert("Delete", "Alarm Deleted!", "Ok");
            await databasReader.DeleteItemAsync(alarm);
            Navigation.PopAsync();
        }
    }
}
