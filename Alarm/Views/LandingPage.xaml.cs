﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Alarm.Utilities;
using Alarm.Utilities.Helpers.DatabaseReader;
using Xamarin.Forms;

namespace Alarm.Views
{
    public partial class LandingPage : ContentPage
    {
        DatabaseReader databaseReader = DatabaseReader.GetInstance;
        public LandingPage()
        {
            InitializeComponent();
            SetAlarms();
        }

        async void SetAlarms()
        {
            //var alarms = databaseReader.FetchData();
            //foreach (var temp in alarms.Result)
            //{
            //    Console.WriteLine("for each");
            //    if (temp.didStart == false)
            //    {
            //        //Console.WriteLine(temp.id + " did start " + temp.didStart);
            //        //CrossLocalNotifications.Current.Show("Alarm", "Bodyyyyyy", temp.id, DateTime.Now.AddMinutes(temp.minutes));
            //        temp.didStart = true;
            //        //Console.WriteLine(temp.id + " did start " + temp.didStart);
            //        //await databaseReader.SaveItemAsync<Models.Alarm>(temp, "Alarm", temp.id);
            //        DependencyService.Get<iLocalNotification>().LocalNotification("title", "body", temp.id, DateTime.Now.AddMinutes(temp.minutes));
            //    }
            //    else
            //    {
            //        Console.WriteLine(temp.id + " did start: " + temp.didStart);
            //    }
            //}
        }

        void AddAlarm_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Views.AddAlarmPage());
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            listView.ItemsSource = null;
            listView.ItemsSource = await databaseReader.FetchData();
        }

        void Alarm_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Models.Alarm alarm = (Models.Alarm)listView.SelectedItem;
            Navigation.PushAsync(new Views.EditAlarmPage(alarm));
            listView.SelectedItem = null;
        }

        async void AlarmSwitch_Toggled(object sender, Xamarin.Forms.ToggledEventArgs e)
        {
            try{
                var setAlarm = ((Switch)sender).BindingContext as Models.Alarm;
                int time = 0;
                if (((Switch)sender).IsToggled)
                {
                    //var hourNow = DateTime.Now.Hour;
                    //var minuteNow = DateTime.Now.Minute;
                    //var secondNow = DateTime.Now.Second;

                    //var timerHour = timePicker.Time.Hours;
                    //var timerMinute = timePicker.Time.Minutes;
                    //var tempHour = (timerHour - hourNow) * 60;

                    //time = (((timerMinute - minuteNow) + tempHour) * 60) - secondNow;
                    //Console.WriteLine("secs " + time);
                    //setAlarm.secondsToAlarm = time;

                    //await databaseReader.SaveItemAsync<Models.Alarm>(setAlarm, "Alarm", setAlarm.id);
                }
                else
                {
                    DependencyService.Get<iLocalNotification>().Cancel(setAlarm.id);
                }
            }catch(NullReferenceException error)
            {
                
            }
        }
    }
}
